from datetime import date
import discord
from discord.ext import commands
from config import settings
import logging
import random
import time
from time import sleep
#import vlc
import asyncio
from yandex_music import Client
import os
from glob import iglob
import shutil
import os
import youtube_dl

bot = commands.Bot(command_prefix = settings['prefix'])
vc = None

youtube_dl.utils.bug_reports_message = lambda: ''
ytdl_format_options = {
    'format': 'bestaudio/best',
    'restrictfilenames': True,
    'noplaylist': True,
    'nocheckcertificate': True,
    'ignoreerrors': False,
    'logtostderr': False,
    'quiet': True,
    'no_warnings': True,
    'default_search': 'auto',
    'source_address': '0.0.0.0' # bind to ipv4 since ipv6 addresses cause issues sometimes
}
ffmpeg_options = {
    'options': '-vn'
}
ytdl = youtube_dl.YoutubeDL(ytdl_format_options)
class YTDLSource(discord.PCMVolumeTransformer):
    def __init__(self, source, *, data, volume=0.5):
        super().__init__(source, volume)
        self.data = data
        self.title = data.get('title')
        self.url = ""
    @classmethod
    async def from_url(cls, url, *, loop=None, stream=False):
        loop = loop or asyncio.get_event_loop()
        data = await loop.run_in_executor(None, lambda: ytdl.extract_info(url, download=not stream))
        if 'entries' in data:
            # take first item from a playlist
            data = data['entries'][0]
        filename = data['title'] if stream else ytdl.prepare_filename(data)
        return filename

@bot.command()
async def leave(ctx):
    global vc
    if vc.is_connected():
        await vc.disconnect()
    else:
        await ctx.send("Мы с тобой не ведем диалог, так сказать, лично...")


@bot.command()
async def help_me(ctx):
    #global vc
    await ctx.send("{}, дорогуша, давай договоримся, как ТЕБЕ лучше общаться со МНОЙ!".format(ctx.author.name))
    await ctx.send("Есть несколько магический слов, чтобы попросить меня о чем-то, даже не предлагая сыр.")
    await ctx.send("================================================================================================")
    await ctx.send("Начнем с простых, базовых магическиъ слов, о смертный, дабы не перегрузит Ваш светлый ум!")
    await ctx.send("'!pause' - ставит то, что я проигрываю на паузу, создавай временной пузырь в пространстве континуума!")
    await ctx.send("'!resume'- разрушает предыдущее заклинание запуская течение времени ровно с того места, где оно остановилось, так что ты еще застанешь дождь из лягушек!")
    await ctx.send("'!stop' - перекрывает даэдрический звуковой поток, прекращая воспроизведение меллодий! Сильное заклинание!")
    await ctx.send("================================================================================================")
    await ctx.send("Чтож, перейдем к заклинаниям посложнее!!!")
    await ctx.send("'!play_local_music' -  Я включу тебе музыку, которая хранится в моем мире! (локально на сервере)")
    await ctx.send("'!play_yandex' -  Я включу тебе музыку, из мира Яндекс.Музыка! Имеется две опции: 'likes_playlist', я включу тебе свои любимы песенки :3 и 'own_playlist|<username>|<kind>' (где <username> - это имя пользователя, а <kind> это идентификатор плейлиста их ЯМ). Пример написания '!play_yandex likes_playlist' или '!play_yandex own_playlist|aakvs|1000'")
    await ctx.send("'!play_youtube <youtube_url>' -  Я включу тебе музыку из мира ютуба, которую ты хочешь послушать, просто дай мне ссылку!")
    await ctx.send("================================================================================================")
    await ctx.send("Очень ваное заявление!!! Прежде чем запрашивать у меня новое воспроизведение, обязательно применяй заклинание '!stop', т.к. даэдрический поток не терпит двех меллодий одновременно!!!")
    await ctx.send("Ну что, все уяснил, СМЕРТНЫЙ?!")

@bot.command()
async def play_youtube(ctx, url):
    #try :
    global vc
    voice_channel = ctx.author.voice.channel
    # async with ctx.typing():
    #     filename = await YTDLSource.from_url(url, loop=bot.loop)
    #     voice_channel.play(discord.FFmpegPCMAudio(executable="ffmpeg.exe", source=filename))
    filename = await YTDLSource.from_url(url, loop=bot.loop)
    if voice_channel != None:
        try:
            vc = await voice_channel.connect()
            vc.play(discord.FFmpegPCMAudio(executable="C:/ffmpeg/bin/ffmpeg.exe", source=filename))#'C:/yml/example.mp3'))
        except:
            vc.play(discord.FFmpegPCMAudio(executable="C:/ffmpeg/bin/ffmpeg.exe", source=filename))#'C:/yml/example.mp3'))
    else:
        await ctx.send(str(ctx.author.name) + ", это не канал связи с даэдра, глупец!!!")
     
    await ctx.send('Сейчас вы изволите слышать сие творение ===> {}'.format(filename))
    #except:
        #await ctx.send("The bot is not connected to a voice channel.")

@bot.command()
async def play_local_music(ctx, url):
    logging.error(url)
    global vc
    PATH = r'E:\Sheogorath-bot\local_music'
    path = 'big_track_{}.mp3'.format(str(date.today()))
    destination = open(path, 'wb')
    for filename in iglob(os.path.join(PATH, '*.mp3')):
        shutil.copyfileobj(open(filename, 'rb'), destination)
    destination.close()
    voice_channel = ctx.author.voice.channel
    if voice_channel != None:
        try:
            vc = await voice_channel.connect()
            vc.play(discord.FFmpegPCMAudio(executable="C:/ffmpeg/bin/ffmpeg.exe", source=path))#'C:/yml/example.mp3'))
        except:
            vc.play(discord.FFmpegPCMAudio(executable="C:/ffmpeg/bin/ffmpeg.exe", source=path))#'C:/yml/example.mp3'))  
    await ctx.message.delete()

# async def play_local_music(ctx):
#     global vc
#     global time_track_start
#     global music_time
#     music_list = os.listdir("./local_music")
#     voice_channel = ctx.author.voice.channel
#     if voice_channel != None:
#         vc = await voice_channel.connect()
#         for music_file in music_list:
#             music_path = "E:/Sheogorath-bot/local_music/{}".format(music_file)
#             f = MP3("E:/Sheogorath-bot/local_music/{}".format(music_file))
#             music_time = int(f.info.length) + 3
#             now = datetime.now()
#             time_track_start = datetime.now()
#             vc.play(discord.FFmpegPCMAudio(executable="C:/ffmpeg/bin/ffmpeg.exe", source=music_path))#'C:/yml/example.mp3'))
#             await asyncio.sleep(music_time)    
#     else:
#         await ctx.send(str(ctx.author.name) + "is not in a channel.")
#     await ctx.message.delete()

@bot.command()
async def play_yandex(ctx, type_option):
    global vc
    client = Client.from_credentials('login', 'password')
    if "trackName" in type_option:
        filename = type_option.split(":")[1]
        voice_channel = ctx.author.voice.channel
        if voice_channel != None:
            vc = await voice_channel.connect()
            vc.play(discord.FFmpegPCMAudio(executable="C:/ffmpeg/bin/ffmpeg.exe", source='E:/Sheogorath-bot/{}'.format(filename)))
        else:
            await ctx.send(str(ctx.author.name) + ", это не канал связи с даэдра, глупец!!!")
        await ctx.message.delete()
    if type_option == "likes_playlist":
        folder_name = "like_playlist"
        list_of_yam_tracks = client.users_likes_tracks()
        num = 0
        for track in list_of_yam_tracks:
            num += 1
            try:
                track.fetch_track().download('E:/Sheogorath-bot/yandex_music/{}/{}.mp3'.format(folder_name, num))
            except:
                continue
        guid = random.randint(100, 1000)
        PATH = r"E:/Sheogorath-bot/yandex_music/" + str(folder_name)
        path = 'big_track_{}{}.mp3'.format(date.today(), guid)
        destination = open(path, 'wb')
        for filename in iglob(os.path.join(PATH, '*.mp3')):
            shutil.copyfileobj(open(filename, 'rb'), destination)
        destination.close()
        #client.users_likes_tracks()[0].fetch_track().download('E:/Sheogorath-bot/yandex_music/example.mp3')
        # client.users_playlists(kind="1000")[0].fetch_track().download('E:/Sheogorath-bot/yandex_music/example2.mp3')
        await asyncio.sleep(5)
        voice_channel = ctx.author.voice.channel
        if voice_channel != None:
            try:
                vc = await voice_channel.connect()
                vc.play(discord.FFmpegPCMAudio(executable="C:/ffmpeg/bin/ffmpeg.exe", source=path))#'C:/yml/example.mp3'))
            except:
                vc.play(discord.FFmpegPCMAudio(executable="C:/ffmpeg/bin/ffmpeg.exe", source=path))#'C:/yml/example.mp3'))  
        else:
            await ctx.send(str(ctx.author.name) + ", это не канал связи с даэдра, глупец!!!")
        await ctx.message.delete()
    if "own_playlist" in type_option:
        username = type_option.split("|")[1]
        kind = type_option.split("|")[2]
        guid = random.randint(100, 1000)
        folder_name = "{}_{}_playlist_{}__{}".format(username, kind, date.today(), guid)
        os.mkdir('E:/Sheogorath-bot/yandex_music/{}'.format(folder_name))
        list_of_yam_tracks = client.users_playlists(kind=kind, user_id=username)["tracks"]
        num = 0
        for track in list_of_yam_tracks:
            num += 1
            track.fetch_track().download('E:/Sheogorath-bot/yandex_music/{}/{}.mp3'.format(folder_name, num))
            if num == 100:
                break
        guid = random.randint(100, 1000)
        PATH = r"E:/Sheogorath-bot/yandex_music/" + str(folder_name)
        path = 'big_track_{}{}.mp3'.format(date.today(), guid)
        destination = open(path, 'wb')
        for filename in iglob(os.path.join(PATH, '*.mp3')):
            shutil.copyfileobj(open(filename, 'rb'), destination)
        destination.close()
        #client.users_likes_tracks()[0].fetch_track().download('E:/Sheogorath-bot/yandex_music/example.mp3')
        # client.users_playlists(kind="1000")[0].fetch_track().download('E:/Sheogorath-bot/yandex_music/example2.mp3')
        await asyncio.sleep(5)
        voice_channel = ctx.author.voice.channel
        if voice_channel != None:
            try:
                vc = await voice_channel.connect()
                vc.play(discord.FFmpegPCMAudio(executable="C:/ffmpeg/bin/ffmpeg.exe", source=path))#'C:/yml/example.mp3'))
            except:
                vc.play(discord.FFmpegPCMAudio(executable="C:/ffmpeg/bin/ffmpeg.exe", source=path))#'C:/yml/example.mp3'))  
        else:
            await ctx.send(str(ctx.author.name) + ", это не канал связи с даэдра, глупец!!!")
        await ctx.message.delete()
    #else:
    #   await ctx.send(str(ctx.author.name) + "Дорогой мой, вы что-то не так мне сказали!!!")
    

@bot.command()
async def resume(ctx):
    voice_client = ctx.message.guild.voice_client
    if voice_client.is_paused():
        voice_client.resume()
    else:
        await ctx.send("Ооо, прости, но я не могу продолжить проиграывать музыку, ты до этого ничего не останавливал, мой друг...")

@bot.command()
async def pause(ctx):
    voice_client = ctx.message.guild.voice_client
    if voice_client.is_playing():
        voice_client.pause()
    else:
        await ctx.send("Друзья, в данный момент, мои даэдрические способности не направлены на воспроизведение музыки!!!")

@bot.command()
async def stop(ctx):
    voice_client = ctx.message.guild.voice_client
    if voice_client.is_playing():
        voice_client.stop()
    else:
        await ctx.send("Друзья, в данный момент, мои даэдрические способности не направлены на воспроизведение музыки!!!")

# async def start_messeage(ctx):
#     await send("Я был призвани в мир живых, о СМЕРТНЫЕ НИЧТОЖЕСТВА!!! Если Вам интересно как лучше строить заклинание для даэдрических подношений, используйте СЛОВО СИЛЫ '!help_me'")


bot.run(settings['token'])

